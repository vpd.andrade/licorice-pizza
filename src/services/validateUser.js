import axios from "axios";

const findUser = async (userEmail, password) => {
  const query = { email : userEmail.value,  password:  password.value}

  try {

    const response = await axios({
      method: 'get',
      url: 'http://localhost:3004',
      params: query,
      
    })

    if(response.status !== 201)
      return {
        valid: false,
        error: 'tente novamente'
      }

      return {
        valid: true,
        data: response.data,
        token: response.token
      };
  } catch (error) {
    console.log("Ocorreu um error: ", error)
    return {
      valid: false,
      error: 'tente novamente'
    }
  }
  
}

const createUser = async data => {
  try {

    const response = await axios({
      method: 'post',
      url: 'http://localhost:3004',
      data,
      
    })

    if(response.status !== 200)
      return {
        valid: false,
        error: 'tente novamente'
      }

      return {
        valid: true,
        data: response.data,
        token: response.token
      };
  } catch (error) {
    console.log("Ocorreu um error: ", error)
    return {
      valid: false,
      error: 'tente novamente'
    }
  }
    
}


export default { findUser, createUser };
