import React from "react";
import userService from "../services/validateUser";
import {  useNavigate } from 'react-router-dom';

export const UserContext = React.createContext();

export const UserStorage = ({ children }) => {
  const [data, setData] = React.useState(null);
  const [error, setError] = React.useState(null);

  const navigate = useNavigate();

  async function userLogin(userEmail, password) {

    const { valid, error: ErrorData, data = {} } = await userService.findUser(userEmail, password);

    if (!valid && ErrorData){ 
        setError(ErrorData);
        return null;
    }
    setData({name: data.name || ''});
    navigate('/')

}

async function createUser(userEmail, password, userName) {
  const dataUser = { email: userEmail.value, password: password.value, name: userName.value };

  const { valid, error: ErrorData, data = {} } = await userService.createUser(dataUser);

  if (!valid && ErrorData){ 
      setError(ErrorData);
      return null;
  }
  setData({name: data.name || ''});
  navigate('/')

}

  return <UserContext.Provider value={{ userLogin, createUser, data, error }}>{children}</UserContext.Provider>;
};
