import React from "react";

const Footer = () => {
  return (
    <footer
      style={{
        position: 'fixed',
        bottom: '0',
        width: '100%',
        height: '130px',
        textAlign: "center",
        padding: "40px",
        fontSize: "40px",
        fontFamily: "Poppins",
        color: "#FEBB41",
        backgroundColor: "#2F2F2F",
      }}
    >
      Licorice pizza
    </footer>
  );
};

export default Footer;
