import React from 'react';

const Body = () => {

    return(
        <body style={{ margin: '0 auto', height: '800px', backgroundColor: '#404040', textAlign: 'center'}}>
            <h1 style={{fontSize: '100px', color: 'white', fontFamily: 'Princess Sofia'}}>Em manutenção</h1>
        </body>
    )

}

export default Body;