import React from "react";
import { Link } from "react-router-dom";
import "../../../src/app.css";
import "../../../src/index.css";
import "./header.css";

import { ReactComponent as Logo } from "../../imgs/Licorice Pizza.svg";
import { UserContext } from "../../contexts/UserContext";

const Header = () => {
  const { data } = React.useContext(UserContext);

  return (
    <header>
      <nav className="container">
        <Link to="/">
          <Logo />
        </Link>
        <nav>
          <Link className="navigation" to="/">
            Home
          </Link>
          <Link className="navigation" to="/">
            Contato
          </Link>
          <Link className="navigation" to="/">
            Delivery
          </Link>
          {data ? (
            <Link className="navigation" to="/conta">
              Olá {data.name}
            </Link>
          ) : (
            <Link className="navigation" to="/login">
              Login
            </Link>
          )}
        </nav>
      </nav>
    </header>
  );
};

export default Header;
