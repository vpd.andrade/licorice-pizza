import React from "react";
import { Link } from "react-router-dom";
import "../../../src/app.css";
import "./login.css";
import "./login.css";
import Input from "./forms/Input";
import Button from "./forms/Button";
import Error from "../../helper/Error";
import useForm from "../../hooks/useForm";
import { UserContext } from "../../contexts/UserContext";

const LoginForm = () => {
  const userEmail = useForm("email");
  const userPasswd = useForm();

  const { userLogin, error } = React.useContext(UserContext);

  const handleSubmit = (event) => {
    event.preventDefault();

    if (userEmail.validate() && userPasswd.validate()) {
      userLogin(userEmail, userPasswd);
    }
  };

  return (
    <section className="container">
      <div className="section-login">
        <h1 className="title">Fazer login</h1>
        <div className="cadastrar">
          <p>Não tenho uma conta, quero me </p>
          <Link style={{paddingLeft: '5px', color: 'black', fontWeight: 'bold' }} to="/login/criar"> cadastrar</Link>
        </div>

        <form className="form" onSubmit={handleSubmit}>
          <Input label="E-mail" type="email" name="useremail" {...userEmail} />
          <Input
            label="Senha"
            type="password"
            name="password"
            {...userPasswd}
          />
          <Button>Entrar</Button>
          {error && <Error error={error} />}
          <div className="lost-pass">
          <Link to="login/perdeu">
            Perdeu a senha?
          </Link>
          </div>

        </form>
      </div>
    </section>
  );
};

export default LoginForm;
