import React from 'react';
import { Route, Routes } from 'react-router-dom';
import  './login.css';
import LoginCreate from './LoginCreate';
import LoginForm from './LoginForm';
import LoginLostPass from './LoginLostPass';
import LoginResetPass from './LoginResetPass';

const Login = () => {

    return (
        <div className='login-section'>

        <Routes>
            <Route path="/*" element={<LoginForm/>}/>
            <Route path="criar" element={<LoginCreate/>}/>
            <Route path="perdeu" element={<LoginLostPass/>}/>
            <Route path="resetar" element={<LoginResetPass/>}/>
        </Routes>

        </div>
    )
}

export default Login;





