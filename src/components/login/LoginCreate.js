import React from "react";
import "../../../src/app.css";
import "./login.css";
import "./login.css";
import Input from "./forms/Input";
import Button from "./forms/Button";
import Error from "../../helper/Error";
import useForm from "../../hooks/useForm";
import { UserContext } from "../../contexts/UserContext";


const LoginCreate = () => {
    const userEmail = useForm("email");
    const userPasswd = useForm();
    const userName = useForm();
  
    const { createUser, error } = React.useContext(UserContext);
  
    const handleSubmit = (event) => {
      event.preventDefault();
  
      if (userEmail.validate() && userPasswd.validate() && userName.validate())
        createUser(userEmail, userPasswd, userName);
      
    };
  
    return (
      <section className="container">
        <div className="section-login">
          <h1 className="title">criar cadastro</h1>
 
          <form className="form" onSubmit={handleSubmit}>
          <Input
              label="Nome"
              type="string"
              name="name"
              {...userName}
            />
            <Input label="E-mail" type="email" name="useremail" {...userEmail} />
            <Input
              label="Senha"
              type="password"
              name="password"
              {...userPasswd}
            />
            <Button>Criar</Button>
            {error && <Error error={error} />}

          </form>
        </div>
      </section>
    );
}

export default LoginCreate;