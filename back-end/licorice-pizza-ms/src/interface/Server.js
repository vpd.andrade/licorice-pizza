const express = require("express");
const bodyParser = require("body-parser");
const cors = require('cors');
const compression = require("compression");
const methodOverride = require("method-override");

const users = [
  {
    email: "vinicius@gmail.com",
    password: "12345",
    name: "Vinicius",
  },
];
const app = () => {
  const app = express();
  
  app
  .use(methodOverride('X-HTTP-Method-Override'))
  .use(bodyParser.json())
  .use(compression())
  .use(cors())
  
  app.get(
    "/",
    function (req, res, next) {
     const { email = {}, password = {} } = req.query || {};
     const userData = users.find(data => data.email === email && data.password === password) || {}
    
     if(userData.name)
        return res.status(201).json({name: userData.name, email: userData.email})

        return res.status(404).json({ message: 'User not found' })
      next();
    }
  );

  app.post("/", (req, res, next) => {
        const userData = req.body || {};
        users.push(userData);

        console.log(users)

        return res.status(200).json(userData)
      next()
  });

  app.listen(3004, () => {
    console.log("app listening on port 3002");
  });

  return app;
};

module.exports = app;
